class User < ActiveRecord::Base
	has_many :poll_votes
	has_many :initiatives, through :poll_votes
end
