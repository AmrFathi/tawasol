class Tag < ActiveRecord::Base
	has_many :taggings
	has_many :initiatives, through: :taggings
end
