class Initiative < ActiveRecord::Base

	# Associations
	
	belongs_to :sector
	has_many :comments
	has_many :polls

	#users vs initiatives
	has_many :poll_votes
	has_many :users, through: :poll_votes
		

	#tagging
	attr_accessible :description, :title, :tag_list, :region, :img, :sector_id
	has_many :taggings
	has_many :tags, through: :taggings

	
	def self.tagged_with(name)
	  Tag.find_by_name!(name).initiatives
	end

	def tag_list
	  tags.map(&:name).join(", ")
	end

	def tag_list=(names)
	  self.tags = names.split(",").map do |n|
	    	Tag.where(name: n.strip).first_or_create!
	  end
	end
	

	#search scope by title & Region of initiative
	scope :searchRegion, lambda{|searchRegion| where (["region LIKE ?","%#{searchRegion}%"])}
	scope :searchTitle, lambda{|searchTitle| where (["title LIKE ?","%#{searchTitle}%"])}


	#paperclip:
	
	# has_attached_file :photo, :styles => { :small => "150x150>" },
	#                   :default_url => "/images/:style/missing.png"

	# validates_attachment_presence :photo
	# validates_attachment_size :photo, :less_than => 5.megabytes
	# validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/png']

end
