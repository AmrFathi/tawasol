class InitiativesController < ApplicationController

	def new
   
	   	@sectors=Sector.all
	  	initiative=Initiative.new
	  	 
	end

	def create
	  	 initiative=Initiative.new(:tag_list => params[:tag_list])
		 initiative.title=params[:title]
	  	 initiative.description=params[:description]
	  	 initiative.img=params[:img]
	  	 initiative.region=params[:region]
	  	 initiative.sector_id=params[:sectorID].to_i
	  	 
	  	 if initiative.save
	  	 	redirect_to(:controller => 'polls', :action => 'new', :initiative_id => initiative.id)
	  	 else
	  	 	render('new')

		end
	end
	
	def list
		@sectors = Sector.all
		case 
		when params[:searchTitle]
			@initiatives = Initiative.searchTitle(params[:searchTitle])
		when params[:tag]
			@initiatives = Initiative.tagged_with(params[:tag])
		when params[:searchRegion]
			@initiatives = Initiative.searchRegion(params[:searchRegion])
		else
			@initiatives = Initiative.all
		end
	end
	
	def show
		@sector = Sector.find(params[:id_sector])
		@polls = Poll.where("initiative_id" => params[:id_initiative])
		@initiative = Initiative.find(params[:id_initiative])
		@comment = Comment.new
		@comment.content = params[:content]
		@comment.commentable_id = params[:id_initiative]
		@comment.commentable_type = "initiative"
		@comment.save
		@comments = Comment.where("commentable_id" => params[:id_initiative]).where("commentable_type" => "initiative")
	end


end
