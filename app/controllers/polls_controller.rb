class PollsController < ApplicationController
  
  # GET /polls
  def index
    @polls = Poll.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /polls/1
  # GET /polls/1.json
  def show
    @poll = Poll.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /polls/new
  # GET /polls/new.json
  def new 
    @poll = Poll.new
    @initiative_id = params[:initiative_id].to_i
     2.times do 
       question = @poll.questions.build
       3.times{question.answers.build}
     end

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /polls/1/edit
  def edit
    @poll = Poll.find(params[:id])
  end

  # POST /polls
  def create
    @poll = Poll.new(params[:poll])


    respond_to do |format|
      if @poll.save
        format.html { redirect_to @poll}
     else
        format.html { render action: "new" }
     end
    end
  end

  # PUT /polls/1
  # PUT /polls/1.json
  def update
    @poll = Poll.find(params[:id])

    respond_to do |format|
      if @poll.update_attributes(params[:poll])
        format.html { redirect_to @poll}
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /polls/1
  def destroy
    @poll = Poll.find(params[:id])
    @poll.destroy

    respond_to do |format|
      format.html { redirect_to polls_url }
    end
  end
end
