class CreatePollVotes < ActiveRecord::Migration
  def change
    create_table :poll_votes do |t|
      t.integer :question_id
      t.integer :answer_id
      t.integer :poll_id
      
      t.belongs_to :initiative
      t.belongs_to :user
      
      t.timestamps
    end
  end
end
