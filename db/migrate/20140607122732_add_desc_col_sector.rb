class AddDescColSector < ActiveRecord::Migration
  def up
  	add_column("sectors","description", :string)
  end

  def down
	remove_column("sectors","description", :string)
  end
end
