class CreateSectors < ActiveRecord::Migration
  def up
    create_table :sectors do |t|
    	t.string :sectorname

      t.timestamps
    end
  end

  def down
  	drop_table :sectors
  end

end
