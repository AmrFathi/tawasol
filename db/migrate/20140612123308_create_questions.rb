class CreateQuestions < ActiveRecord::Migration
  def up
    create_table :questions do |t|
      t.text :content
      t.integer :poll_id

      t.timestamps
    end
    def down
    	drop_table(:questions)
    end
  end
end
