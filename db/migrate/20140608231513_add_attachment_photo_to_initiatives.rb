class AddAttachmentPhotoToInitiatives < ActiveRecord::Migration
  def self.up
    change_table :initiatives do |t|
      t.attachment :photo
      #added: photo_file_name, content_type, file_size,  photo_updated_at
    end
  end

  def self.down
    drop_attached_file :initiatives, :photo
  end
end
