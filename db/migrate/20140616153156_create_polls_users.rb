class CreatePollsUsers < ActiveRecord::Migration
  def change
    create_table :polls_users do |t|
      t.integer :initiative_id
      t.integer :question_id
      t.integer :answer_id
      t.integer :poll_id
      t.integer :user_id
      t.timestamps
    end
  end
end
