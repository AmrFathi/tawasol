class CreateInitiatives < ActiveRecord::Migration
  def up
    create_table :initiatives do |t|
      t.string :title
      t.text :description
      t.string :img
      t.string :region
      t.integer :vote

      #sector_id as a foreign key from sector table
      t.references :sector 

      t.timestamps
    end
  end

def down
  drop_table :initiatives

end

end
